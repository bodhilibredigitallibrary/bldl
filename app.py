from flask import Flask, render_template
import os
from csv import reader
import pandas as pd


app = Flask(__name__, static_folder='static')

def kiwixServer(): 
    path = '/media/aaru/DL-1/' # path where are the zim files are stored
    files = []
    # r=root, d=directories, f = files
    for r, d, f in os.walk(path): #For loop to read all zim file in the directory(path)
        for file in f:
            if file.endswith('.zim'):
                files.append(os.path.join(r, file))

    zimfile = ' '.join(files)
    print(zimfile)
    os.system('./kiwix-serve --port=8090 -v '+ zimfile)








csvFilePath = os.getcwd() +'/static/contents.csv'
new = pd.read_csv(csvFilePath)

title = []
fileType = []
image = []
file = []
desc = []
url = []
lang = []

with open(csvFilePath, 'r', newline='') as read_obj:
    csv_reader = reader(read_obj)
    objList = list(csv_reader)
    for recordList in objList:
        i = 0
        for each in recordList:
            i = i+1
            if (i==1):
                title.append(each)
            elif (i==2):
                fileType.append(each)
            elif(i==3):
                image.append(each)
            elif(i==4):
                file.append(each)
            elif(i==5):
                desc.append(each)
            elif(i==6):
                url.append(each)
            else:
                lang.append(each)
    print(title)
    print(url)





@app.route('/')
def index():
    kiwixServer()
    return render_template('index.html',title =title, image= image, url= url)


@app.route('/download')
def download():
    return render_template('download.html', len = len(objList), recordList = objList)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5060, debug=True)